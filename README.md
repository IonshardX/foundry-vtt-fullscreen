# Foundry VTT - Fullscreen

![Fullscreen Menu Preview](https://gitlab.com/Ionshard/foundry-vtt-fullscreen/-/wikis/uploads/7fd584a4aafa4b0f672c60c8162429c3/fvtt-fullscreen.png)

## Install

### Foundry Add-On Module Installation

- Go to Foundry VTT's Configuration and Setup Menu
- Select the Add-on Modules tab
- Select the Install Module button
- Use the link [https://gitlab.com/Ionshard/foundry-vtt-fullscreen/-/jobs/artifacts/master/raw/module.json?job=build-module](https://gitlab.com/Ionshard/foundry-vtt-fullscreen/-/jobs/artifacts/master/raw/module.json?job=build-module) as the Manifest URL
- Select Install

### Manual Install

Download the [fullscreen.zip](https://gitlab.com/Ionshard/foundry-vtt-fullscreen/-/jobs/artifacts/master/raw/fullscreen.zip?job=build-module) module and extract it to the `data/modules` directory inside Foundry VTT's user data directory.

## License
<a rel="license" href="https://spdx.org/licenses/MIT.html"><img alt="MIT License" style="border-width:0" src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/License_icon-mit-88x31-2.svg/88px-License_icon-mit-88x31-2.svg.png" /></a> Fullscreen - a module for Foundry VTT - by <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/Ionshard/foundry-vtt-anvil-menu" property="cc:attributionName" rel="cc:attributionURL">Victor Ling</a> is licensed under an <a rel="license" href="https://spdx.org/licenses/MIT.html"> MIT License</a>.

This work is licensed under the Foundry Virtual Tabletop [Limited License Agreement for Module Development](https://foundryvtt.com/article/license/).


