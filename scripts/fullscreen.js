(() => {

  const CONFIG = {
    NAME: 'fullscreen',
    SETTINGS: {
      REBIND: 'rebind',
      ESCAPE_KEY: 'escapeKey'
    }
  };

  // Settings

  function getRebind() {
    game.settings.get(CONFIG.NAME, CONFIG.SETTINGS.REBIND)
  }

  function setRebind(value) {
    game.settings.set(CONFIG.NAME, CONFIG.SETTINGS.REBIND, value);
  }

  function setEscapeKey(key) {
    game.settings.set(CONFIG.NAME, CONFIG.SETTINGS.ESCAPE_KEY, key);
  }

  function getEscapeKey() {
    if (getRebind() === 'no')
      return null;

    return game.settings.get(CONFIG.NAME, CONFIG.SETTINGS.ESCAPE_KEY);
  }

  function onKeyDown(event) {
    const key = getEscapeKey()
    if (!key || keyboard.getKey(event) !== key) return;

    keyboard._handleKeys(event, "Escape", false);
  }

  function onKeyUp(e) {
    const key = getEscapeKey()
    if (!key || keyboard.getKey(event) !== key) return;

    keyboard._handleKeys(event, "Escape", true);
  }

  function handleListeners() {
    window.addEventListener('keydown', onKeyDown);
    window.addEventListener('keyup', onKeyUp);
  }

  function toggleFullScreen() {
    if (!document.fullscreenElement) {
      document.documentElement.requestFullscreen();
    } else {
      if (document.exitFullscreen) {
        document.exitFullscreen();
      }
    }
  }

  function registerSettings() {
    game.settings.register(CONFIG.NAME, CONFIG.SETTINGS.REBIND, {
      name: 'Rebind Escape Key (Experimental)',
      hint: 'Binds the Escape Key to another key to prevent exiting fullscreen mode.',
      scope: 'client',
      config: true,
      type: String,
      default: 'no',
      choices: {
        no: 'Don\'t Rebind Escape',
        backquote: 'Rebind to Backquote (`) - QWERTY Keyboards',
        lessthansign: 'Rebind to Less Than Sign (<) - AZERTY & QWERTZ Keyboards',
        custom: 'Rebind to a custom Key'
      },
      onChange: selection => {
        if (selection === 'backquote') {
          setEscapeKey('`');
        }
        else if (selection === 'lessthansign') {
          setEscapeKey('<');
        }
        else if (selection === 'custom') {
          new Dialog({
            title: 'Enter Custom Key to Rebind Escape',
            content: '<form> <div class="form-group"> <label>New Key</label> <input type="text" name="key" value="" placeholder="`"/> </div> </form>',
            buttons: {
              save: {
                icon: '<i class="fas fa-check"></i>',
                label: "Save",
                callback: html => {
                  const key = html.find('[name="key"]').val();
                  setEscapeKey(key);
                }
              },
              cancel: {
                icon: '<i class="fas fa-times"></i>',
                label: "Cancel",
                callback: () => {
                  setRebind('no');
                  setEscapeKey(null);
                }
              }
            },
            default: "save"
          }).render(true);
        }
        else {
          setEscapeKey(null);
        }
      }
    });

    game.settings.register(CONFIG.NAME, CONFIG.SETTINGS.ESCAPE_KEY, {
      name: "Rebound Escape Key",
      scope: "client",
      config: false,
      type: String,
      default: null,
    });
  }

  Hooks.on('ready', () => {
    registerSettings();
    handleListeners();

    AnvilMenu.registerMenuEntries([
      {
        name: 'Foundry Menu',
        icon: '<i class="fas fa-bars"></i>',
        callback: () => ui.menu.toggle()
      },
      {
       icon: '<i class="fas fa-expand"></i>',
       name: 'Toggle Fullscreen',
       callback: () => toggleFullScreen()
      }
    ]);
  });
})();
